import {isEmpty} from '../../src';
import assert from 'assert';

describe('Common#isEmpty', function() {
    it('should {} equal true', function () {
        assert.strictEqual(isEmpty({}), true);
    });
    it('should null equal true', function () {
        assert.strictEqual(isEmpty(null), true);
    });
    it('should empty arry equal true', function () {
        assert.strictEqual(isEmpty([]), true);
    });
    it('should empty string equal true', function () {
        assert.strictEqual(isEmpty(''), true);
    });
});