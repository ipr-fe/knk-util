import {trim} from '../../src';
import assert from 'assert';

describe('Common#trim', function() {
    const str = '  knk trim   ';
    it('should trim all', function () {
        assert.strictEqual(trim(str), 'knktrim');
    });
    it('should trim before', function () {
        assert.strictEqual(trim(str, 'before'), 'knk trim   ');
    });
    it('should trim after', function () {
        assert.strictEqual(trim(str, 'after'), '  knk trim');
    });
    it('should trim both', function () {
        assert.strictEqual(trim(str, 'both'), 'knk trim');
    });
});