import {getEnumsArray} from '../../src';
import assert from 'assert';

describe('Common#getEnumsArray', function() {
    const gender = {
        '0': 'male',
        '1': 'female'
    };
    it('should convert obj to array', function () {
        const result = getEnumsArray(gender);
        const arry =  [{ text: 'male', value: '0' }, { text: 'female', value: '1' }];
        assert.strictEqual(result.toString(), arry.toString());
    });
});