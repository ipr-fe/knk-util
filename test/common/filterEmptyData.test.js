import {filterEmptyData} from '../../src';
import assert from 'assert';

describe('Common#filterEmptyData', function() {
    it('should filtered empty data', function () {
        const filterData = {
            userId: 1,
            userName: null,
            gender: undefined,
            tel: '',
            addr: '重庆'
        };
        const filteredData = {
            userId: 1,
            addr: '重庆'
        };
        const result = filterEmptyData(filterData);
        assert.strictEqual(result.toString(), filteredData.toString());
    });
});