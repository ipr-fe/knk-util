---
title: dataType 数据类型判断
group:
  title: 通用函数
---

## dataType 数据类型判断

```
/**
 *  数据类型判断
 */
export default function dataType (target) {
    let type = Object.prototype.toString.call(target).slice(8, -1);
    let typeObj = {
        'Object': true,
        'Array': true,
        'Function': true,
        'RegExp': true,
        'Date': true,
        'Error': true,
        'Arguments': true,
        'Set': true,
        'Map': true,
        'Promise': true,
        'Symbol': true,
        'BigInt': true,
        'Math': true,
        'Null': true,
        'Undefined': true,
        'String': true,
        'Number': true,
        'Boolean': true
    };
    let val = {
        type
    };
    val['is' + type] = typeObj[type];
    if(type === 'String') {
        try {
            let obj = JSON.parse(target);
            val['isJSON'] = obj && typeof obj == 'object' ? true: false;
        }catch(e) {
            val['isJSON'] = false;
        }
    }
    return val;                                                                                          
};
```