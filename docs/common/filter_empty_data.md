---
title: filterEmptyData 过滤空数据
group:
  title: 通用函数
---

## filterEmptyData 过滤空数据

```
/**
 * 过滤空数据
 */
export function filterEmptyData(data) {
    if(!data) return data;
    Object.keys(data).forEach((inx) => {
        if (data[inx] === 'undefined' || data[inx] === undefined || data[inx] === null || data[inx] === '' || data[inx].length == 0)
            delete data[inx];
    });
    return data;
}
```
