import dataType from "./dataType";

/**
 *  验证两个对象的值是否一样, 返回第一个不一样的结果, 适用于值为基本数据类型的对象
 *  @param obj1         比较对象一
 *  @param obj2         比较对象二
 */
export default function isObjEqual (obj1, obj2) {
    if(!dataType(obj1).isObject || !dataType(obj2).isObject) {
        return {
            result: false,
            message: '比较对象其一为空！'
        };
    }

    if(Object.keys(obj1).length !== Object.keys(obj2).length) {
        return {
            result: false,
            message: '比较对象数据长度不一致！'
        };
    }

    for(let i in obj1) {
        if(obj1[i] !== obj2[i]) {
            return {
                result: false,
                message: 'obj1.'+i +'：'+  (obj1[i] || undefined) + ' 不等于 obj2.' + i +'：'+ (obj2[i] || undefined)
            };
        }
    }
    return {
        result: true,
        message: '相等'
    };
};