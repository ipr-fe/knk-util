/**
 * 通过枚举获取数组
 * @param  {Object} enumObj 枚举对象
 * @return {Array}          数组
 */
export function getEnumsArray(enumObj) {
    return Object.keys(enumObj).map(function (key) {
        return {
            text: enumObj[key],
            value: key,
        };
    });
}
