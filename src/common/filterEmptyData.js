/**
 * 过滤空数据
 */
export function filterEmptyData(data) {
    if (!data) return data;
    const filterData = data;
    Object.keys(filterData).forEach((inx) => {
        if (filterData[inx] === 'undefined' ||
            filterData[inx] === undefined ||
            filterData[inx] === null ||
            filterData[inx] === '' ||
            filterData[inx].length === 0
        )
            delete filterData[inx];
    });
    return filterData;
}
