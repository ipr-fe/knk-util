import dataType from "./dataType";

/**
 *  判断数据是否为空      undefined、null、''、false、0
 *   @param isBool      为false时是否算空                            默认不做判断
 *   @param isZero      为数字零时是否算空   包含数字零和字符串零      默认不做判断
 */
export default function isEmpty ({ data, isBool = false, isZero = false }) {
    // 判断是否是空数组
    if(dataType(data).isArray && !data.length) {
        return true;
    }
    // 判断是否是空对象
    if(dataType(data).isObject && !Object.keys(data).length ) {
        return true;
    }
    // 判断数据值
    let emptyList = [ undefined, null, '', 'undefind', 'null' ];
    isBool ? emptyList.push(false, 'false') : '';
    isZero ? emptyList.push(0, '0') : '';
    return emptyList.includes(data);
};