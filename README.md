# knk-util

[![npm version][npm-image]][npm-url] [![mnt-image](https://img.shields.io/maintenance/yes/2022.svg?style=flat-square)](../../commits/master) [![Gitee stars](https://gitee.com/ipr-fe/knk-util/badge/star.svg?theme=dark)](https://gitee.com/artdong/vue-admin/stargazers) [![dumi](https://img.shields.io/badge/docs%20by-dumi-blue?style=flat-square)](https://gitee.com/umijs/dumi) [![Gitee license](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](https://gitee.com/ipr-fe/knk-util/blob/master/LICENSE)

[npm-image]: http://img.shields.io/npm/v/knk-util.svg?style=flat-square
[npm-url]: http://npmjs.org/package/knk-util
[download-image]: https://img.shields.io/npm/dm/knk-util.svg?style=flat-square
[download-url]: https://npmjs.org/package/knk-util
[bundlephobia-url]: https://bundlephobia.com/result?p=knk-util
[bundlephobia-image]: https://badgen.net/bundlephobia/minzip/knk-util

> 是一个一致性、模块化、高性能的 JavaScript 实用工具库。

## 📍 如何使用

### 安装

```
# use npm
npm i knk-util

# use yarn
yarn add knk-util
```

### 加入开发

#### 本地预览

```
# use npm
npm install
npm start

# use yarn
yarn install
yarn start
```

#### 发布文档

```
# use npm
npm run deploy

# use yarn
yarn deploy
```

#### 打包发布

```
# use npm
npm run compile

# use yarn
yarn compile
```

## LICENSE

[MIT](https://gitee.com/ipr-fe/knk-util/blob/master/LICENSE)